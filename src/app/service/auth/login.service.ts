import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {UserModel} from '../../model/authenticacion/user-model';
import {catchError} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class LoginService {
    API_ENDPOINT = window.location.origin;
    ipAddress: any;
    constructor(private httpClient: HttpClient) {
        /*this.httpClient.get<{ip: string}>('https://jsonip.com')
            .subscribe( data => {
                console.log('th data', data);
                this.ipAddress = data;
            });*/

    }

    validarUsuario(user: UserModel): Observable <any> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'applicationCode': 'SABM',
                'channelCode': 'SMP',
                'macAddress': '1000000000'
            })
        };
        return this.httpClient.post(this.API_ENDPOINT + '/login/v1/validateUser', JSON.stringify(user), httpOptions).pipe(
            catchError(this.handleError<any>('validarUsuario', [])));
    }

    private handleError<T>(operation = 'operation', result?: T) {

        return (error: any): Observable<T> => {
            console.error(error);
            this.log(`${operation} failed: ${error.message}`);

            return of(result as T);
        };
    }

    private log(message: string) {
        console.log(message);
    }
}
