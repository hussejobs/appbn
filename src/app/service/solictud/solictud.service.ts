import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {UserModel} from '../../model/authenticacion/user-model';
import {Observable, of} from 'rxjs';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SolictudService {
    API_ENDPOINT = window.location.origin;
  constructor(private httpClient: HttpClient) { }

    listaServicio(): Observable <any> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };
        return this.httpClient.get(this.API_ENDPOINT + '/login/v1/validateUser', httpOptions).pipe(
            catchError(this.handleError<any>('validarUsuario', [])));
    }

    private handleError<T>(operation = 'operation', result?: T) {

        return (error: any): Observable<T> => {
            console.error(error);
            this.log(`${operation} failed: ${error.message}`);

            return of(result as T);
        };
    }

    private log(message: string) {
        console.log(message);
    }
}
