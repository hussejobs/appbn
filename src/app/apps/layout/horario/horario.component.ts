import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatDialogRef, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

@Component({
    selector: 'app-horario',
    templateUrl: './horario.component.html',
    styleUrls: ['./horario.component.scss'],
    animations: [routerTransition()]
})
export class HorarioComponent implements OnInit {
    form: FormGroup;
    panelOpenState = false;
    displayedColumns: string[] = [
        'descripcion',
        'dia',
        'horaInicio',
        'horaFin',
        'modificar'
    ];
    dataSource = new MatTableDataSource();
    @ViewChild('HorarioSort', { static: true, read: MatSort }) horarioSort: MatSort;
    @ViewChild('HorarioPaginator', { static: true, read: MatPaginator }) horarioPaginator: MatPaginator;

    constructor(private _formBuilder: FormBuilder, public dialog: MatDialog) {

        this.dataSource = new MatTableDataSource(ELEMENT_DATA);
        this.dataSource.sort = this.horarioSort;
        this.dataSource.paginator = this.horarioPaginator;
    }

    ngOnInit() {
        // Reactive Form
        this.form = this._formBuilder.group({
            company: [
                {
                    value: '0502545889987 - OASYSTEMS S.R.L.',
                    disabled: false
                }, Validators.required
            ],
            firstName: ['', Validators.required],
            date: ['', Validators.required],
            lastName: ['', Validators.required],
            address: ['', Validators.required],
            address2: ['', Validators.required],
            city: ['', Validators.required],
            state: ['', Validators.required],
            postalCode: ['', [Validators.required, Validators.maxLength(5)]],
            country: ['', Validators.required]
        });
    }

    modificar(item) {

        console.log(item);

        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.data = {
           

        };

        const dialogRef = this.dialog.open(HorarioDialog, dialogConfig);

        dialogRef.afterClosed().subscribe(result => {

            if (result !== '') {
                console.log(JSON.stringify(result));
                
            }
        });
    }

}

export interface HorarioElement {
    descripcion: string;
    dia: string;
    horaInicio: string;
    horaFin: string;
}

const ELEMENT_DATA: HorarioElement[] = [
    {
        descripcion: 'HORARIO PAGO LUNES',
        dia: 'LUN',
        horaInicio: '08:00',
        horaFin: '16:00',
    },
    {
        descripcion: 'HORARIO PAGO MARTES',
        dia: 'MAR',
        horaInicio: '08:00',
        horaFin: '16:00',
    },
    {
        descripcion: 'HORARIO PAGO MIERCOLES',
        dia: 'MIE',
        horaInicio: '08:00',
        horaFin: '16:00',
    },
    {
        descripcion: 'HORARIO PAGO JUEVES',
        dia: 'JUE',
        horaInicio: '08:00',
        horaFin: '16:00',
    },
    {
        descripcion: 'HORARIO PAGO VIERNES',
        dia: 'VIE',
        horaInicio: '08:00',
        horaFin: '16:00',
    },
    {
        descripcion: 'HORARIO PAGO SABADO',
        dia: 'SAB',
        horaInicio: '08:00',
        horaFin: '16:00',
    },
    {
        descripcion: 'HORARIO PAGO DOMINGO',
        dia: 'DOM',
        horaInicio: '08:00',
        horaFin: '16:00',
    },
    
];

@Component({
    selector: 'horariodialog',
    templateUrl: 'horario.modificar.html'
})
export class HorarioDialog implements OnInit {
   
    formHorario: FormGroup;

    constructor(private _formBuilder: FormBuilder
        , private dialogRef: MatDialogRef<HorarioDialog>) {


    }


    ngOnInit() {

       this.formHorario = this._formBuilder.group({
            firstName: ['', Validators.required],
            company: ['', Validators.required],
            country: ['', Validators.required]
        }); 

    }

    save() {
        console.log(JSON.stringify(this.formHorario.value));
        this.dialogRef.close(this.formHorario.value);

    }

}
