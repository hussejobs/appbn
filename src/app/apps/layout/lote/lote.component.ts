import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatDialogRef, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

@Component({
    selector: 'app-lote',
    templateUrl: './lote.component.html',
    styleUrls: ['./lote.component.scss'],
    animations: [routerTransition()]
})
export class LoteComponent implements OnInit {
    form: FormGroup;
    panelOpenState = false;
    displayedColumns: string[] = [
        'seleccionar',
        'idServicio',
        'nroLote',
        'entidad',
        'registros',
        'lote',
        'idEstado',
        'fechaRegistro',
        'transaccion',
        'tipoPlanilla',
        'importe',
        'tipoEjecucion',
        'detalle',
        'aprobar',
        'anular'
    ];
    dataSource = new MatTableDataSource();
    @ViewChild('loteSort', { static: true, read: MatSort }) loteSort: MatSort;
    @ViewChild('lotePaginator', { static: true, read: MatPaginator }) lotePaginator: MatPaginator;

    constructor(private _formBuilder: FormBuilder, public dialog: MatDialog) {

        this.dataSource = new MatTableDataSource(ELEMENT_DATA);
        this.dataSource.sort = this.loteSort;
        this.dataSource.paginator = this.lotePaginator;
    }

    ngOnInit() {
        // Reactive Form
        this.form = this._formBuilder.group({
            company: [
                {
                    value: '0502545889987 - OASYSTEMS S.R.L.',
                    disabled: false
                }, Validators.required
            ],
            firstName: ['', Validators.required],
            date: ['', Validators.required],
            lastName: ['', Validators.required],
            address: ['', Validators.required],
            address2: ['', Validators.required],
            city: ['', Validators.required],
            state: ['', Validators.required],
            postalCode: ['', [Validators.required, Validators.maxLength(5)]],
            country: ['', Validators.required]
        });
    }

    aprobar(item) {
    }

    anular(item) {
    }

    detalle(item) {

        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.data = {
           

        };

        const dialogRef = this.dialog.open(LoteDialog, dialogConfig);

        dialogRef.afterClosed().subscribe(result => {

            if (result !== '') {
                console.log(JSON.stringify(result));
                /*this.perfilService.add(result).subscribe(
                    res => {
                        console.log(res);
                        if (res != null) {
                            this._snackBar.open('Registro Correcto...!!!', 'Cerrar [x]', {
                                duration: 2000,
                                horizontalPosition: this.horizontalPosition,
                                verticalPosition: this.verticalPosition,
                            });
                            this.getPerfiles();
                        }

                    },
                    err => {
                        console.log('errorr');
                    }
                );*/
            }
        });
    }

}

export interface LoteElement {
    idServicio: string;
    nroLote: string;
    entidad: string;
    registros: number;
    lote: number;
    idEstado: string;
    fechaRegistro: Date;
    nroSubCuenta: string;
    idSIAF: string;
    cartaOrden: string;
    transaccion: string;
    tipoPlanilla: string;
    importe: string;
}

const ELEMENT_DATA: LoteElement[] = [
    {
        idServicio: 'ABONO SERVICIO',
        nroLote: '0000000',
        entidad: 'H',
        registros: 3,
        fechaRegistro: new Date(),
        lote: 1,
        idEstado: 'REGISTRADO',
        nroSubCuenta: ' ',
        idSIAF: '',
        cartaOrden: '',
        transaccion: '',
        tipoPlanilla: '',
        importe: '15,00'
    },
    {
        idServicio: 'ABONO SERVICIO',
        nroLote: '0000000',
        entidad: 'H',
        registros: 3,
        fechaRegistro: new Date(),
        lote: 1,
        idEstado: 'REGISTRADO',
        nroSubCuenta: ' ',
        idSIAF: '',
        cartaOrden: '',
        transaccion: '',
        tipoPlanilla: '',
        importe: '15,00'
    },
    {
        idServicio: 'ABONO SERVICIO',
        nroLote: '0000000',
        entidad: 'H',
        registros: 3,
        fechaRegistro: new Date(),
        lote: 1,
        idEstado: 'REGISTRADO',
        nroSubCuenta: ' ',
        idSIAF: '',
        cartaOrden: '',
        transaccion: '',
        tipoPlanilla: '',
        importe: '15,00'
    },
    {
        idServicio: 'ABONO SERVICIO',
        nroLote: '0000000',
        entidad: 'H',
        registros: 3,
        fechaRegistro: new Date(),
        lote: 1,
        idEstado: 'REGISTRADO',
        nroSubCuenta: ' ',
        idSIAF: '',
        cartaOrden: '',
        transaccion: '',
        tipoPlanilla: '',
        importe: '15,00'
    },
    {
        idServicio: 'ABONO SERVICIO',
        nroLote: '0000000',
        entidad: 'H',
        registros: 3,
        fechaRegistro: new Date(),
        lote: 1,
        idEstado: 'REGISTRADO',
        nroSubCuenta: ' ',
        idSIAF: '',
        cartaOrden: '',
        transaccion: '',
        tipoPlanilla: '',
        importe: '15,00'
    },
];

@Component({
    selector: 'lotedialog',
    templateUrl: 'lotedetalle.html'
})
export class LoteDialog implements OnInit {
   
    formLoteDetalle: FormGroup;

    constructor(private _formBuilder: FormBuilder
        // , @Inject(MAT_DIALOG_DATA) { des_corta_perfil, des_larga_perfil, codigo_estado, usuario_creacion, perfil_id }: PerfilModel
        , private dialogRef: MatDialogRef<LoteDialog>) {


    }


    ngOnInit() {

       this.formLoteDetalle = this._formBuilder.group({
            firstName: ['', Validators.required],
            company: ['', Validators.required],
            country: ['', Validators.required]
        }); 

    }

    save() {
        console.log(JSON.stringify(this.formLoteDetalle.value));
        this.dialogRef.close(this.formLoteDetalle.value);

    }

}
