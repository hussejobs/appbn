import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {routerTransition} from '../../../router.animations';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatDialogRef, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';

@Component({
    selector: 'app-lotetransmitido',
    templateUrl: './lotetransmitido.component.html',
    styleUrls: ['./lotetransmitido.component.scss'],
    animations: [routerTransition()]
})
export class LoteTransmitidoComponent implements OnInit {
    form: FormGroup;
    panelOpenState = false;
 
    displayedColumns: string[] = [
        'idServicio',
        'nroLote',
        'estado',
        'fechaRegistro',
        'aprobar',
        'anular'
    ];
    dataSource = new MatTableDataSource();
    @ViewChild('loteSort', {static: true, read: MatSort}) loteSort: MatSort;
    @ViewChild('lotePaginator', {static: true, read: MatPaginator}) lotePaginator: MatPaginator;

    constructor(private _formBuilder: FormBuilder,  public dialog: MatDialog) {

        this.dataSource = new MatTableDataSource(ELEMENT_DATA);
        this.dataSource.sort = this.loteSort;
        this.dataSource.paginator = this.lotePaginator;
    }

    ngOnInit() {
        // Reactive Form
        this.form = this._formBuilder.group({
            company: [
                {
                    value: '0502545889987 - OASYSTEMS S.R.L.',
                    disabled: true
                }, Validators.required
            ],
            firstName: ['', Validators.required],
            date: ['', Validators.required],
            lastName: ['', Validators.required],
            address: ['', Validators.required],
            address2: ['', Validators.required],
            city: ['', Validators.required],
            state: ['', Validators.required],
            postalCode: ['', [Validators.required, Validators.maxLength(5)]],
            country: ['', Validators.required]
        });
    }

    aprobar(item) {
    }

    anular(item) {
    }

    detalle(item) {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.data = {
         /*   des_corta_perfil: this.perfilObj.des_corta_perfil,
            des_larga_perfil: this.perfilObj.des_larga_perfil,
            codigo_estado: this.perfilObj.codigo_estado,
            usuario_creacion: "SPARTANO-CODE",
            perfil_id: this.perfilObj.perfil_id*/

        };

        const dialogRef = this.dialog.open(LoteTransmitidoDialog, dialogConfig);

        dialogRef.afterClosed().subscribe(result => {

            if (result !== '') {
                console.log(JSON.stringify(result));
                /*this.perfilService.add(result).subscribe(
                    res => {
                        console.log(res);
                        if (res != null) {
                            this._snackBar.open('Registro Correcto...!!!', 'Cerrar [x]', {
                                duration: 2000,
                                horizontalPosition: this.horizontalPosition,
                                verticalPosition: this.verticalPosition,
                            });
                            this.getPerfiles();
                        }

                    },
                    err => {
                        console.log('errorr');
                    }
                );*/
            }
        });
    }

}

 

export interface LoteElement {
    idServicio: string;
    nroLote: string;
    entidad: string;
    estado: string;
    fechaRegistro: Date;
}
 // servicio , num lote, entidad , fecha registro , estado host
const ELEMENT_DATA: LoteElement[] = [
    {
        idServicio: 'ABONO SERVICIO',
        nroLote: '0000000',
        entidad: 'H',
        fechaRegistro: new Date(),
        estado: 'REGISTRADO'
    },
    {
        idServicio: 'ABONO SERVICIO',
        nroLote: '0000000',
        entidad: 'H',
        fechaRegistro: new Date(),
        estado: 'REGISTRADO'
    },
    {
        idServicio: 'ABONO SERVICIO',
        nroLote: '0000000',
        entidad: 'H',
        fechaRegistro: new Date(),
        estado: 'REGISTRADO'
    },
    {
        idServicio: 'ABONO SERVICIO',
        nroLote: '0000000',
        entidad: 'H',
        fechaRegistro: new Date(),
        estado: 'REGISTRADO'
    },
    {
        idServicio: 'ABONO SERVICIO',
        nroLote: '0000000',
        entidad: 'H',
        fechaRegistro: new Date(),
        estado: 'REGISTRADO'
    },
];

@Component({
    selector: 'lotetransmitidodialog',
    templateUrl: 'lotetransmitidoDialog.html'
})
export class LoteTransmitidoDialog implements OnInit {
    form: FormGroup;

    constructor(private _formBuilder: FormBuilder
        // , @Inject(MAT_DIALOG_DATA) { des_corta_perfil, des_larga_perfil, codigo_estado, usuario_creacion, perfil_id }: PerfilModel
        , private dialogRef: MatDialogRef<LoteTransmitidoDialog>) {


       /* this.form = this._formBuilder.group({
            des_corta_perfil: [des_corta_perfil, Validators.required],
            des_larga_perfil: [des_larga_perfil, Validators.required],
            codigo_estado: [codigo_estado, Validators.required],
            usuario_creacion,
            perfil_id
        });*/

    }


    ngOnInit() {
        // Reactive Form

    }

    save() {
        console.log(JSON.stringify(this.form.value));
        this.dialogRef.close(this.form.value);

    }

}
