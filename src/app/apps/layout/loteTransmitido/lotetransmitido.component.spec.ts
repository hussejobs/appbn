import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoteTransmitidoComponent } from './lotetransmitido.component';

describe('LoteComponent', () => {
  let component: LoteTransmitidoComponent;
  let fixture: ComponentFixture<LoteTransmitidoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoteTransmitidoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoteTransmitidoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
