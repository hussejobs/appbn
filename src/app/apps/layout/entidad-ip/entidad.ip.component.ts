import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatDialogRef, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

@Component({
    selector: 'app-entidad-ip',
    templateUrl: './entidad.ip.component.html',
    styleUrls: ['./entidad.ip.component.scss'],
    animations: [routerTransition()]
})
export class EntidadIPComponent implements OnInit {
  
    panelOpenState = false;
    displayedColumns: string[] = [
        'cuenta',
        'entidad',
        'ip',
        'modificar',
        'eliminar'
    ];
    dataSource = new MatTableDataSource();
    @ViewChild('HorarioSort', { static: true, read: MatSort }) horarioSort: MatSort;
    @ViewChild('HorarioPaginator', { static: true, read: MatPaginator }) horarioPaginator: MatPaginator;

    constructor(public dialog: MatDialog) {

        this.dataSource = new MatTableDataSource(ELEMENT_DATA);
        this.dataSource.sort = this.horarioSort;
        this.dataSource.paginator = this.horarioPaginator;
    }

    ngOnInit() {
 
    }

    buscarCuenta(){
        console.log("buscarCuenta");

        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.data = {
           

        };

        const dialogRef = this.dialog.open(BusquedaCuentaDialog, dialogConfig);

        dialogRef.afterClosed().subscribe(result => {

            if (result !== '') {
                console.log(JSON.stringify(result));
                
            }
        });
    }

    eliminarIP(item){

        console.log("eliminarIP");
        console.log(item);

        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.data = {
           

        };

        const dialogRef = this.dialog.open(EliminarIpDialog, dialogConfig);

        dialogRef.afterClosed().subscribe(result => {

            if (result !== '') {
                console.log(JSON.stringify(result));
                
            }
        });
    }

    cambio: boolean = true;

    

    administraIP(item) {

        console.log("administrarIP");
        console.log(item);

        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.data = {
           

        };

        const dialogRef = this.dialog.open(AdministrarIpDialog, dialogConfig);

        dialogRef.afterClosed().subscribe(result => {

            if (result !== '') {
                console.log(JSON.stringify(result));
                
            }
        });
    }

}

export interface EntidadElement {
    
    cuenta: string;
    entidad: string;
    ip: string;
}

const ELEMENT_DATA: EntidadElement[] = [
    {
        cuenta:'0545sfb',
        entidad: 'ENTIDAD 1',
        ip: '10.0.0.0'
    },
    {
        cuenta:'0545sfb',
        entidad: 'ENTIDAD 2',
        ip: '10.0.0.0'
    },
    {
        cuenta:'0545sfb',
        entidad: 'ENTIDAD 3',
        ip: '10.0.0.0'
    },
    
];


export interface BusquedaCuentaElement {
    descripcion: string;
    cuenta: string;
}

const ELEMENT_DATA_BUSQUEDA: BusquedaCuentaElement[] = [
    {
        descripcion: 'DESCRIP1',
        cuenta: '4564363'
    },
    {
        descripcion: 'DESCRIP2',
        cuenta: '4564363'
    },
    {
        descripcion: 'DESCRIP3',
        cuenta: '4564363'
    },
    
];



@Component({
    selector: 'administrardialog',
    templateUrl: 'entidad.ip.administrar.html'
})
export class AdministrarIpDialog implements OnInit {
   
    formAdministrarIP: FormGroup;

    constructor(private _formBuilder: FormBuilder
        , private dialogRef: MatDialogRef<AdministrarIpDialog>) {


    }


    ngOnInit() {

       this.formAdministrarIP = this._formBuilder.group({
            firstName: ['', Validators.required],
            company: ['', Validators.required],
            country: ['', Validators.required]
        }); 

    }

    cambio:boolean = false;

    cambioIp(event){
        console.log(event);

        if(event.value == 1){
            this.cambio = false;
        }else{
            this.cambio = true;
        }
        
    }

    save() {
        console.log(JSON.stringify(this.formAdministrarIP.value));
        this.dialogRef.close(this.formAdministrarIP.value);

    }

}



@Component({
    selector: 'eliminaripdialog',
    templateUrl: 'entidad.ip.eliminar.html'
})
export class EliminarIpDialog implements OnInit {
   
    formEliminarIP: FormGroup;

    constructor(private _formBuilder: FormBuilder
        , private dialogRef: MatDialogRef<EliminarIpDialog>) {


    }


    ngOnInit() {

       this.formEliminarIP = this._formBuilder.group({
            firstName: ['', Validators.required],
            company: ['', Validators.required],
            country: ['', Validators.required]
        }); 

    }

    save() {
        console.log(JSON.stringify(this.formEliminarIP.value));
        this.dialogRef.close(this.formEliminarIP.value);

    }

}


@Component({
    selector: 'busquedacuentadialog',
    templateUrl: 'busqueda.cuenta.html'
})
export class BusquedaCuentaDialog implements OnInit {
   
    formBusquedaCuenta: FormGroup;

    

    displayedColumnsBusqueda: string[] = [
        'seleccionar',
        'cuenta',
        'descripcion'
    ];

    

    dataSource = new MatTableDataSource();
    @ViewChild('BusquedaCuentaSort', { static: true, read: MatSort }) busquedaCuentaSort: MatSort;
    @ViewChild('BusquedaCuentaPaginator', { static: true, read: MatPaginator }) busquedaCuentaPaginator: MatPaginator;


    constructor(
        private dialogRef: MatDialogRef<BusquedaCuentaDialog>) {
            
            this.dataSource = new MatTableDataSource(ELEMENT_DATA_BUSQUEDA);
            this.dataSource.sort = this.busquedaCuentaSort;
            this.dataSource.paginator = this.busquedaCuentaPaginator;

    }


    ngOnInit() {

    }

    save() {
        console.log(JSON.stringify(this.formBusquedaCuenta.value));
        this.dialogRef.close(this.formBusquedaCuenta.value);

    }

}