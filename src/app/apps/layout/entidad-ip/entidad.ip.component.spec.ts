import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntidadIPComponent } from './entidad.ip.component';

describe('EntidadIPComponent', () => {
  let component: EntidadIPComponent;
  let fixture: ComponentFixture<EntidadIPComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EntidadIPComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntidadIPComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
