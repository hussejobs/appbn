import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HeaderComponent } from './components/header/header.component';
import {SolicitudComponent, SolicitudDialog} from './solicitud/solicitud.component';
import {LoteComponent, LoteDialog} from './lote/lote.component';
import {LoteTransmitidoComponent,LoteTransmitidoDialog } from './loteTransmitido/lotetransmitido.component';
import {ReporteAbonoAprobadoComponent } from './reporte-abono-aprobado/reporte-abono-aprobado.component';
import {ReporteAbonoAnuladoComponent } from './reporte-abono-anulado/reporte-abono-anulado.component';
import {ReporteAbonoTurboAprobadoComponent } from './reporte-turbo-aprobado/reporte-turbo-aprobado.component';
import { HorarioComponent, HorarioDialog } from './horario/horario.component';
import { EntidadIPComponent, EliminarIpDialog, AdministrarIpDialog, BusquedaCuentaDialog } from './entidad-ip/entidad.ip.component';

import { PageHeaderModule } from '../../shared';

import { AngularDualListBoxModule } from 'angular-dual-listbox';


import {
    MatCardModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    MatSnackBarModule
} from '@angular/material';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatStepperModule } from '@angular/material/stepper';
import { ReactiveFormsModule} from '@angular/forms';
import { SatDatepickerModule, SatNativeDateModule} from 'saturn-datepicker';

import {MatRadioModule} from '@angular/material/radio';


@NgModule({
    imports: [
        CommonModule,
        LayoutRoutingModule,
        TranslateModule,
        NgbDropdownModule,
        PageHeaderModule,
        MatTableModule,
        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatCardModule,
        MatDatepickerModule,
        MatExpansionModule,
        ReactiveFormsModule,
        SatDatepickerModule,
        SatNativeDateModule,
        MatSortModule,
        MatPaginatorModule,
        MatDialogModule,
        MatRadioModule,
        MatSnackBarModule,

        AngularDualListBoxModule
    ],
    declarations: [LayoutComponent, SidebarComponent, HeaderComponent,
        SolicitudComponent, SolicitudDialog,LoteComponent,LoteDialog,HorarioDialog, EliminarIpDialog, AdministrarIpDialog
    ,LoteTransmitidoComponent,LoteTransmitidoDialog, BusquedaCuentaDialog,
    ReporteAbonoAprobadoComponent, ReporteAbonoAnuladoComponent, ReporteAbonoTurboAprobadoComponent, 
    HorarioComponent, EntidadIPComponent],
    entryComponents: [
        SolicitudDialog,
        LoteDialog, HorarioDialog,EliminarIpDialog, AdministrarIpDialog, BusquedaCuentaDialog,
        LoteTransmitidoDialog
    ]
})
export class LayoutModule {}
