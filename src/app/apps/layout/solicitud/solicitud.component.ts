import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {routerTransition} from '../../../router.animations';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatDialogRef, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';

@Component({
    selector: 'app-solicitud',
    templateUrl: './solicitud.component.html',
    styleUrls: ['./solicitud.component.scss'],
    animations: [routerTransition()]
})
export class SolicitudComponent implements OnInit {
    form: FormGroup;
    panelOpenState = false;
    displayedColumns: string[] = ['idServicio',
        'nroSolicitud',
        'entidad',
        'registros',
        'lote',
        'idEstado',
        'fechaRegistro',
        /* 'nroSubCuenta',
         'idSIAF',
         'cartaOrden',*/
        'transaccion',
        'tipoPlanilla',
        'importe',
        'detalle',
        'aprobar',
        'anular'
    ];
    dataSource = new MatTableDataSource();
    @ViewChild('solicitudSort', {static: true, read: MatSort}) solicitudSort: MatSort;
    @ViewChild('solicitudPaginator', {static: true, read: MatPaginator}) solicitudPaginator: MatPaginator;

    constructor(private _formBuilder: FormBuilder,  public dialog: MatDialog) {

        this.dataSource = new MatTableDataSource(ELEMENT_DATA);
        this.dataSource.sort = this.solicitudSort;
        this.dataSource.paginator = this.solicitudPaginator;
    }

    ngOnInit() {
        // Reactive Form
        this.form = this._formBuilder.group({
            company: [
                {
                    value: '0502545889987 - OASYSTEMS S.R.L.',
                    disabled: true
                }, Validators.required
            ],
            firstName: ['', Validators.required],
            date: ['', Validators.required],
            lastName: ['', Validators.required],
            address: ['', Validators.required],
            address2: ['', Validators.required],
            city: ['', Validators.required],
            state: ['', Validators.required],
            postalCode: ['', [Validators.required, Validators.maxLength(5)]],
            country: ['', Validators.required]
        });
    }

    aprobar(item) {
    }

    anular(item) {
    }

    detalle(item) {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.data = {
         /*   des_corta_perfil: this.perfilObj.des_corta_perfil,
            des_larga_perfil: this.perfilObj.des_larga_perfil,
            codigo_estado: this.perfilObj.codigo_estado,
            usuario_creacion: "SPARTANO-CODE",
            perfil_id: this.perfilObj.perfil_id*/

        };

        const dialogRef = this.dialog.open(SolicitudDialog, dialogConfig);

        dialogRef.afterClosed().subscribe(result => {

            if (result !== '') {
                console.log(JSON.stringify(result));
                /*this.perfilService.add(result).subscribe(
                    res => {
                        console.log(res);
                        if (res != null) {
                            this._snackBar.open('Registro Correcto...!!!', 'Cerrar [x]', {
                                duration: 2000,
                                horizontalPosition: this.horizontalPosition,
                                verticalPosition: this.verticalPosition,
                            });
                            this.getPerfiles();
                        }

                    },
                    err => {
                        console.log('errorr');
                    }
                );*/
            }
        });
    }

}

export interface SolicitudElement {
    idServicio: string;
    nroSolicitud: string;
    entidad: string;
    registros: number;
    lote: number;
    idEstado: string;
    fechaRegistro: Date;
    nroSubCuenta: string;
    idSIAF: string;
    cartaOrden: string;
    transaccion: string;
    tipoPlanilla: string;
    importe: string;
}

const ELEMENT_DATA: SolicitudElement[] = [
    {
        idServicio: 'ABONO SERVICIO',
        nroSolicitud: '0000000',
        entidad: 'H',
        registros: 3,
        fechaRegistro: new Date(),
        lote: 1,
        idEstado: 'REGISTRADO',
        nroSubCuenta: ' ',
        idSIAF: '',
        cartaOrden: '',
        transaccion: '',
        tipoPlanilla: '',
        importe: '15,00'
    },
    {
        idServicio: 'ABONO SERVICIO',
        nroSolicitud: '0000000',
        entidad: 'H',
        registros: 3,
        fechaRegistro: new Date(),
        lote: 1,
        idEstado: 'REGISTRADO',
        nroSubCuenta: ' ',
        idSIAF: '',
        cartaOrden: '',
        transaccion: '',
        tipoPlanilla: '',
        importe: '15,00'
    },
    {
        idServicio: 'ABONO SERVICIO',
        nroSolicitud: '0000000',
        entidad: 'H',
        registros: 3,
        fechaRegistro: new Date(),
        lote: 1,
        idEstado: 'REGISTRADO',
        nroSubCuenta: ' ',
        idSIAF: '',
        cartaOrden: '',
        transaccion: '',
        tipoPlanilla: '',
        importe: '15,00'
    },
    {
        idServicio: 'ABONO SERVICIO',
        nroSolicitud: '0000000',
        entidad: 'H',
        registros: 3,
        fechaRegistro: new Date(),
        lote: 1,
        idEstado: 'REGISTRADO',
        nroSubCuenta: ' ',
        idSIAF: '',
        cartaOrden: '',
        transaccion: '',
        tipoPlanilla: '',
        importe: '15,00'
    },
    {
        idServicio: 'ABONO SERVICIO',
        nroSolicitud: '0000000',
        entidad: 'H',
        registros: 3,
        fechaRegistro: new Date(),
        lote: 1,
        idEstado: 'REGISTRADO',
        nroSubCuenta: ' ',
        idSIAF: '',
        cartaOrden: '',
        transaccion: '',
        tipoPlanilla: '',
        importe: '15,00'
    },
];

@Component({
    selector: 'solicituddialog',
    templateUrl: 'solicituddialog.html'
})
export class SolicitudDialog implements OnInit {
    form: FormGroup;

    constructor(private _formBuilder: FormBuilder
        // , @Inject(MAT_DIALOG_DATA) { des_corta_perfil, des_larga_perfil, codigo_estado, usuario_creacion, perfil_id }: PerfilModel
        , private dialogRef: MatDialogRef<SolicitudDialog>) {


       /* this.form = this._formBuilder.group({
            des_corta_perfil: [des_corta_perfil, Validators.required],
            des_larga_perfil: [des_larga_perfil, Validators.required],
            codigo_estado: [codigo_estado, Validators.required],
            usuario_creacion,
            perfil_id
        });*/

    }


    ngOnInit() {
        // Reactive Form

    }

    save() {
        console.log(JSON.stringify(this.form.value));
        this.dialogRef.close(this.form.value);

    }

}
