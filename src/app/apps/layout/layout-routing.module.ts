import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { SolicitudComponent } from './solicitud/solicitud.component';
import { LoteComponent } from './lote/lote.component';
import { LoteTransmitidoComponent} from './loteTransmitido/lotetransmitido.component';
import { ReporteAbonoAprobadoComponent } from './reporte-abono-aprobado/reporte-abono-aprobado.component';
import { ReporteAbonoAnuladoComponent } from './reporte-abono-anulado/reporte-abono-anulado.component';
import { ReporteAbonoTurboAprobadoComponent } from './reporte-turbo-aprobado/reporte-turbo-aprobado.component';
import { HorarioComponent } from './horario/horario.component';
import { EntidadIPComponent } from './entidad-ip/entidad.ip.component';



const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'prefix' },
            { path: 'layout', component: LayoutComponent},
            { path: 'solicitud', component: SolicitudComponent},
            { path: 'lote', component: LoteComponent},
            { path: 'lotetransmitido', component: LoteTransmitidoComponent},

            { path: 'reporte-abono-aprobado', component : ReporteAbonoAprobadoComponent},
            { path: 'reporte-abono-anulado', component : ReporteAbonoAnuladoComponent},
            { path: 'reporte-turbo-aprobado', component : ReporteAbonoTurboAprobadoComponent},
            { path: 'administrar-horario', component : HorarioComponent},
            { path: 'entidad-ip', component : EntidadIPComponent}

            
            

        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
