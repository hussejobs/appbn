import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatDialogRef, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

@Component({
    selector: 'app-reporte-abono-anulado',
    templateUrl: './reporte-abono-anulado.component.html',
    styleUrls: ['./reporte-abono-anulado.component.scss'],
    animations: [routerTransition()]
})
export class ReporteAbonoAnuladoComponent implements OnInit {
    form: FormGroup;

    private chessmen: Array<any> = [
		{ _id: 1, name: 'Lima 1' },
		{ _id: 2, name: 'Lima 2' },
		{ _id: 3, name: 'Arequipa 3' },
		{ _id: 4, name: 'Tacna' },
		{ _id: 5, name: 'Trujillo' },
		{ _id: 6, name: 'San Martin' }
    ];
    
    tab = 1;
	keepSorted = true;
	key: string;
	display: any;
	filter = false;
	source: Array<any>;
	confirmed: Array<any>;
	userAdd = '';
	disabled = false;

	sourceLeft = true;
	private sourceChessmen: Array<any>;
	private confirmedChessmen: Array<any>;

    arrayType = [
		{ name: 'Chessmen', detail: '(object array)', value: 'chess' },
	];

	type = this.arrayType[0].value;
    panelOpenState = false;
    
    constructor(private _formBuilder: FormBuilder, public dialog: MatDialog) {

       
    }


    private useChessmen() {
		this.key = '_id';
		this.display = 'name';
		this.keepSorted = false;
		this.source = this.sourceChessmen;
		this.confirmed = this.confirmedChessmen;
	}

    ngOnInit() {
        // Reactive Form
        this.form = this._formBuilder.group({
            tipoUbicacion: ['', Validators.required],
            servicio: ['', Validators.required],
            date: ['', Validators.required],
            transaccion: ['', Validators.required],
            tipoPlanilla: ['', Validators.required],
            //postalCode: ['', [Validators.required, Validators.maxLength(5)]],
            
        });

        this.doReset();
    }

    
    swapSource() {
		switch (this.type) {
		case this.arrayType[0].value:
			this.useChessmen();
			break;
		}
	}

	doReset() {
		this.sourceChessmen = JSON.parse(JSON.stringify(this.chessmen));
		this.confirmedChessmen = new Array<any>();

		switch (this.type) {
		
		case this.arrayType[0].value:
			this.useChessmen();
			break;
		
		}
	}

    doAdd() {
		for (let i = 0, len = this.source.length; i < len; i += 1) {
			const o = this.source[i];
			const found = this.confirmed.find( (e: any) => e === o );
			if (!found) {
				this.confirmed.push(o);
				break;
			}
		}
	}

	doRemove() {
		if (this.confirmed.length > 0) {
			this.confirmed.splice(0, 1);
		}
	}

}

