import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteAbonoAprobadoComponent } from './reporte-abono-aprobado.component';

describe('LoteComponent', () => {
  let component: ReporteAbonoAprobadoComponent;
  let fixture: ComponentFixture<ReporteAbonoAprobadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReporteAbonoAprobadoComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteAbonoAprobadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
