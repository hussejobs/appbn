import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {routerTransition} from '../../router.animations';
import {UserModel} from '../../model/authenticacion/user-model';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {LoginService} from '../../service/auth/login.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    user: UserModel = new UserModel();
    loginForm: FormGroup;

    constructor(
        private _router: Router, private loginService: LoginService
    ) {
        this.user = new UserModel();
    }

    ngOnInit() {
        this.loginForm = new FormGroup({
            servicio: new FormControl('', [
                Validators.required
            ]),
            usuario: new FormControl('', [
                Validators.required
            ]),
            clave: new FormControl('', [
                Validators.required
            ])
        });
    }

    onLoggedin() {
        localStorage.setItem('isLoggedin', 'true');
    }

    login() {
        this.loginService.validarUsuario(this.user).subscribe(
            res => {
                if ( res.body != null && res.statusCodeValue === 200 ) {
                    sessionStorage.setItem('currentUser', JSON.stringify(res.body.data.loginAdmin));
                    this._router.navigate(['/layout']);
                } else {
                    sessionStorage.setItem('currentUser', null);
                }
            },
            err => {
                console.log('errorr' + err);
            }
        );
    }
}
