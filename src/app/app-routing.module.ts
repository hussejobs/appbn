import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AuthGuard } from './apps/shared';

const routes: Routes = [
    { path: 'auth', loadChildren: () => import('./apps/login/login.module').then(m => m.LoginModule) },
    { path: 'layout', loadChildren: () => import('./apps/layout/layout.module').then(m => m.LayoutModule) },
    { path: '', redirectTo: 'auth/login', pathMatch: 'full' },
    { path: '**', redirectTo: 'auth/login' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
